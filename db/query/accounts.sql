-- name: CreateAccount :one
INSERT INTO accounts (
  name, country, plan, is_active
) VALUES (
  $1, $2, $3, $4
) RETURNING *;