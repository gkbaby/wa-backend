CREATE TABLE "users" (
  "id" int PRIMARY KEY,
  "phone" int NOT NULL,
  "hashed_password" varchar NOT NULL,
  "username" varchar NOT NULL,
  "created_at" varchar NOT NULL,
  "scopes" varchar NOT NULL,
  "first_name" varchar NOT NULL,
  "last_name" varchar,
  "account" int NOT NULL,
  "is_active" bool NOT NULL
);

CREATE TABLE "accounts" (
  "id" int PRIMARY KEY,
  "name" varchar NOT NULL,
  "country" varchar,
  "plan" int NOT NULL,
  "is_active" bool
);

CREATE TABLE "plans" (
  "id" int PRIMARY KEY,
  "amount" float,
  "validity" float,
  "is_active" bool,
  "description" varchar
);

CREATE TABLE "validity" (
  "id" int PRIMARY KEY,
  "account" int,
  "due_on" timestamp
);

CREATE TABLE "paymenthistory" (
  "id" int PRIMARY KEY,
  "account" int,
  "plan" int,
  "paid_on" timestamp,
  "paymentDetail" int
);

CREATE TABLE "paymentgateway" (
  "id" int PRIMARY KEY,
  "account" int,
  "status" bool,
  "gatewayResult" varchar,
  "transaction_id" varchar,
  "method" varchar,
  "partner" varchar
);

CREATE TABLE "messages" (
  "id" int PRIMARY KEY,
  "message" varchar,
  "status" varchar,
  "sent" bool,
  "is_verified" bool,
  "account" int,
  "recieved_on" timestamp,
  "sent_on" timestamp
);

CREATE TABLE "keys" (
  "account" int,
  "key" varchar,
  "value" varchar,
  "is_active" bool
);

ALTER TABLE "users" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "accounts" ADD FOREIGN KEY ("plan") REFERENCES "plans" ("id");

ALTER TABLE "validity" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "paymentgateway" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "paymenthistory" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "keys" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "messages" ADD FOREIGN KEY ("account") REFERENCES "accounts" ("id");

ALTER TABLE "paymenthistory" ADD FOREIGN KEY ("plan") REFERENCES "plans" ("id");

ALTER TABLE "paymenthistory" ADD FOREIGN KEY ("paymentDetail") REFERENCES "paymentgateway" ("id");
