.PHONY: dropdb, createdb, migrate, migratedown

createdb:
	docker exec -it b7b8ae9b53ec createdb --username=root	 --owner=root pigeondb

dropdb:
	docker exec -it b7b8ae9b53ec dropdb  --username=pigeon --owner=root pigeondb

migrate: 
	migrate -path db/migration -database "postgresql://wherenow_local:52270d383d54ea17jhabsd@localhost:5432/pigeon?sslmode=disable" -verbose up
migratedown:
	migrate -path db/migration -database "postgresql://wherenow_local:52270d383d54ea17jhabsd@localhost:5432/pigeon?sslmode=disable" -verbose down